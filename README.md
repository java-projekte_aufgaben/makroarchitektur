# Makroarchitektur

# Inhaltsverzeichnis
- [Makroarchitektur](#makroarchitektur)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Aufgabe Makroarchitektur Teil 1](#aufgabe-makroarchitektur-teil-1)
  - [Aufgabe 1](#aufgabe-1)
  - [Allgemein](#allgemein)
    - [**Layerd Architektur**](#layerd-architektur)
    - [**Ports & Adapter**](#ports--adapter)
      - [**Vorteile**](#vorteile)
      - [**Nachteile**](#nachteile)
  - [Gliederung](#gliederung)
  - [Ports des Ordermanagement](#ports-des-ordermanagement)
    - [Wichtiges am Rande](#wichtiges-am-rande)
- [Code Frontend Svelte](#code-frontend-svelte)
  - [App.svelte](#appsvelte)
  - [Webshop](#webshop)
  - [Lager](#lager)
  - [Bestellung](#bestellung)
  - [Buchhaltung](#buchhaltung)

# Aufgabe Makroarchitektur Teil 1

- Abgabe der Architekturanalyse des bestehenden erplite-Backends
- A1) Dokumentation (schriftlich, Codeauszüge, C4-Diagramme, Klassendiagramme) der Ports-Und-Adapters-Architektur von Ordermanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind: Bestellung aufgeben Bestellung auf bezahlt setzen Packliste generieren Packlistenitems abhaken Bestellung auf IN_DELIVERY setzen wenn alle Packlistenitems gepackt sind.
- A2) Dokumentation (schriftlich, Codeauszüge, Diagramme, C4-Diagramme, Klassendiagramme) der "Architektur" von Stockmanagement anhand der gegebenen Anwendungsfälle, die schon implementiert sind: Packingliste anlegen Packingitems als verpackt markieren.
- B) Abgabe des Frontends für das bestehende erplite-Backend vollständiger Code des Frontends Screenshots und Beschreibung der Funktionalität Codeerklärungen für implementierte Anwendungsfälle

<br>

## Aufgabe 1

Dokumentation zu Ordermanagement:


## Allgemein

In dem Projekt erplite2V2 wird die Implementation der Ports & Adapter sehr ersichtlich dargestellt um ein besseres Verständniss für DDD zu bekommen. 
Es folgen **Layerd Architekture** sowie **Ports & Adapter**.



### **Layerd Architektur**

Das Layerd Architektur ist einer der am weitesten verbreiteten Baustile. Die Idee hinter Layered Architecture ist, dass Module oder Komponenten mit ähnlichen Funktionalitäten in horizontalen Schichten organisiert werden. Infolgedessen erfüllt jede Schicht eine bestimmte Rolle innerhalb der Anwendung. Der mehrschichtige Architekturstil hat keine Beschränkung hinsichtlich der Anzahl der Schichten, die die Anwendung haben kann, da der Zweck darin besteht, Schichten zu haben, die das Konzept der Trennung von Bedenken fördern. Der geschichtete Architekturstil abstrahiert die Sicht auf das System als Ganzes und bietet gleichzeitig genügend Details, um die Rollen und Verantwortlichkeiten der einzelnen Schichten und die Beziehung zwischen ihnen zu verstehen.

![Layerd_Architektur](images/layerd.jpg)


### **Ports & Adapter**

Das Ports- und Adapter-Architekturmuster definiert den Grundsatz, dass ein System nu auf einem einheitlichen Weg genutzt werden darf. 
Und dies unabhängig davon, ob die Nutzung durch einen Menschen, ein Softwareprogramm, einen Test oder einen Batchlauf geschieht. Um dies zu erreichen, bedarf es einer klaren Trennung zwischen der fachlichen Domäne und der Infrastrukturkomponente. Die Domäne selbst nutzt auch Infrastruktur, etwa für das Speichern von Daten. Zwischen der Domäne und der Infrastruktur befinden sich Adapter, die auf Ports der Domäne adaptieren. Dadurch wird die Dömäne unabhängig.

![Ports & Adapter](images/Ports&Adapter.jpg)

#### **Vorteile**
<br>

+ **Klare Einteilung der Komponenten:**
  +  Der Einsatz des Patterns führt zu einer eindeutigen Einteilung der unterschiedlichen Komponenten. Alle Objekte, die beispielsweise für die Darstellung der Anwendung gebraucht werden, existieren lediglich im entsprechenden Gui-Adapter, während die für die Kommunikation mit der Datenbank notwendigen Objekte nur dem Datenbankadapter bekannt sind.
  
  <br>

+ **Eine Anwendung in mehreren Ausprägungen:** 
  + Ähnlich wie in einem Baukastenprinzip lassen sich in der Hexagonalen Architektur verschiedene Varianten einer Anwendung zusammenfügen. So wäre es denkbar den Postgres-Datenbank-Adapter durch einen Android-SQLite-Adapter und den HTML-UI-Adapter durch einen nativen Android-UI-Adapter zu ersetzen. Mit dem Austausch von lediglich zwei Komponenten wandelt man die Anwendung zu einer nativen Android-Anwendung, ohne dass die eigentliche Geschäftslogik geändert werden muss.

<br>

+ **Parallele Entwicklung:**
  + Die klare Einteilung in Komponenten und in wohldefinierten Schnittstellen ermöglicht es verschiedenen Teams oder Einzelpersonen, parallel an der jeweiligen Implementierung zu arbeiten. Zum Zeitpunkt des Entwicklungsstarts ist es einfach das Verhalten noch fehlender Komponenten mit "Test-Adaptern" zu simulieren. So muss man nicht auf die Fertigstellung beispielsweise der Datenhaltungsschicht warten.

<br>

+ **Austauschbarkeit der Adapter:** 
  + Sollte es notwendig sein, Funktionalität auszutauschen, beispielsweise das Datenbanksystem zu ersetzen, darf sich aus Anwendungssicht nicht zwangsweise auch das Protokoll zwischen der Anwendung und der Datenhaltungskomponente ändern. Durch den Einsatz von "Ports and Adapters" kann man dieses Risiko minimieren. Adapter sind leicht austauschbar. Daher ist ein Wechsel zwischen Datenbanken nichts weiter als ein Wechsel des entsprechenden Adapters.

<br>

+ **Einfache Erweiterbarkeit und Skalierbarkeit:**
   +  Durch das konsequente Entwickeln gegen Ports lässt sich die grundlegende Adapterstruktur leicht ändern. So wäre es denkbar, die einzelnen Adapter des Systems als Microservices zu realisieren. Ohne größeren Aufwand ist es so möglich, eine Anwendung von einem lokalem zu einem verteilten System zu migrieren.

<br>

+ **Keine Festlegung auf eine Zulieferform:** 
  + Da die Hexagonale Architektur(Ports & Adapters) keinerlei Annahmen über die Darstellung der Daten und die Entgegennahme der Benutzereingaben trifft, ist man folglich an dieser Stelle auch nicht festgelegt. Hat man seine Anwendung ursprünglich als schichtenbasierte Webanwendung entworfen, so ist es mitunter schwer, ihre Funktionalität einer mobilen Anwendung zur Verfügung zu stellen. Im Falle der Hexagonalen Architektur stellt die mobile Anwendung nur einen weiteren Adapter für die Entgegennahme der Benutzereingaben dar.

<br>

#### **Nachteile**

<br>

+ **Höhere Komplexität:** 
  + Durch den Einsatz jeder Abstraktion erhöht sich zwangsläufig die Komplexität der betroffenen Anwendung. Anders als bei einer monolithischen Anwendung befinden sich die einzelnen Komponenten des Gesamtsystems mitunter nicht mehr an einem Ort. Möglicherweise sind sie über mehrere Projekte und Subprojekte verteilt, was dazu führt, dass sich neue Kollegen zunächst mit der Orientierung etwas schwerer tun.
  
<br>

+ **Orchestrierung der Anwendung notwendig:**
  + Um die einzelnen Teile der Anwendung zusammenzufügen, bedarf es einer Komponente für diese Aufgabe. Diese kann ihrer Aufgabe manuell oder per Dependency Injection nachkommen. Letzteres wiederum erhöht die Komplexität zusätzlich.

<br>

## Gliederung

In diesem Teil wird die Struktur des Ordermanagement genauer analysiert.

+ oerdermanagement
  + api
  + db
  + domain
    + domainevents
    + valueobjects
  + messaging.spring
  + ports
    + in
    + out  
  + services
    + exceptions
    + mapper

<br>

+ Gesamtansicht (sehr stark verkleinert)
![Ordermanagement](images/ordermanagement.jpg)

+ Ansicht der einzelnen Teile (Interfaces/Klassen usw..)
![Ordermanagement](images/ordermanagement_base.jpg)

<br>

## Ports des Ordermanagement

Im "port" package des Porjektes werden jeweils zwei weitere Packages angegeben. 
Die **IN & Out** packages, welche wichtige Interfaces zur Verfügung stellen.

<br>

![Ports](images/ports.jpg)


### Wichtiges am Rande

+ Im sharedkernel Package befinden sich weitere Komponenten, auf welche das gesamte Projekt zugriff hat und ist außerdem ein Teil des DDD.

+ DTOS ausserhalb, innerhalb Domäne (als Domänenmodel) selber, Persistenzmodel vom REPO

+ Kommunikation über SpringEvents (Framework)

+ ApplicationEventPublisher  / ApplicationEventListener

+ Aggregat = Fixer zustand

+ Bei DDD geht man davon aus, dass der größte Teil der Komplexität einer Software nicht in der technischen Umsetzung liegt, sondern in der Modellierung der Domäne.
<br><br>

# Code Frontend Svelte

## App.svelte

```js
<svelte:head>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</svelte:head>

<script>
  import { Router, Link, Route } from "svelte-routing";
  import Bestellstatus from "./routes/Bestellstatus.svelte";
  import Webshop from "./routes/webshop.svelte";
  import Bestellung from "./routes/Bestellung.svelte";
  import Lager from "./routes/Lager.svelte";
  import Buchhaltung from "./routes/Buchhaltung.svelte";
  
  export const url = "";
   
</script>

<main>	

<Router url = "{url}">
  

  <div class="container-fluid">
    <h1 style="margin-left: 10px">Welcome to Ordermanagement</h1>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
          <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <Link class="nav-link active" to="webshop">Webshop</Link>
              </li>
              <li class="nav-item active">
                <Link class="nav-link active" to="bestellstatus">Bestellstatus</Link>
              </li>
              <li class="nav-item active">
                <Link class="nav-link active" to="bestellung">Bestellung</Link>
              </li>
              <li class="nav-item active">
                <Link class="nav-link active" to="lager">Lager</Link>
              </li>
              <li class="nav-item active">
                <Link class="nav-link active" to="buchhaltung">Buchhaltung</Link>
              </li>
          </ul>    
      </div>    
    </nav>    
    
  
    <div>
        <Route path="bestellstatus" component={Bestellstatus} />
        <Route path="webshop" component={Webshop} />
        <Route path="bestellung" component={Bestellung} />
        <Route path="lager" component={Lager} />
        <Route path="buchhaltung" component={Buchhaltung} />
    </div>
  </div>    
</Router>

</main>

<style></style>
```

## Webshop

```js
<svelte:head>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
</svelte:head>


<script>
	let cart = [];
	let products = [
		{productNumber: "D123RE123D", productName: "HP Pavilion", tax: 1.2, image: "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_88440190/fee_786_587_png", priceNet: 1229, amount: 1},
		{productNumber: "D123RE123A", productName: "MacBook Pro", tax: 1.2, image: "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_73406796/fee_786_587_png", priceNet: 4454, amount: 1},
		{productNumber: "D123RE123B", productName: "Playstation 5", tax: 1.2, image: "https://www.esdorado.com/3701-large_default/console-sony-playstation-5-ps5.jpg", priceNet: 749.99, amount: 1},
	]

    let customer = {
    customerID: "1234567890",
    customerFirstname: "Michael",
    customerLastname: "Beer",
    customerEmail: "mibeer@tsn.at",
    customerStreet: "Blvd of broken dreams",
    customerZipcode: "90048",
    customerCity: "Los Angeles",
    customerCountry: "California",
    cartItems: [],
  };
  let result = null;


    async function buy() {
    cart = parseCart(cart);
    customer.cartItems = cart;
    const res = await fetch("http://localhost:8080/api/v1/orders/", {
      method: "POST",
      headers: {
        "Content-Type": "application/hal+json",
      },
      body: JSON.stringify(customer),
    });
    const json = await res.json();
    result = JSON.stringify(json);
    cart = [];
  }

  function parseCart(cart) {
    let newCart = [];
    let newItem;

    for (let item of cart) {
      newItem = {
        productNumber: item.productNumber,
        productName: item.productName,
        priceNet: item.priceNet,
        tax: item.tax,
        amount: item.quantity,
      };
      newCart = [...newCart, newItem];
    }
    return newCart;
  }

	
	const addToCart = (product) => {
        for(let item of cart) {
            if(item.productNumber === product.productNumber) {
                product.amount += 1
                cart = cart;
                console.log(cart);
                return;
            }
        }
        cart = [...cart, product]
    }

    const minusItem = (product) => {
        for(let item of cart) {
            if(item.productNumber === product.productNumber) {
                if(product.amount > 1 ) {
                    product.amount -= 1
                    cart = cart
                } else {
                    cart = cart.filter((cartItem) => cartItem != product)
                }
                return;
            }
        }
    }

    const plusItem = (product) => {
        for(let item of cart) {
            if(item.productNumber === product.productNumber) {
                item.amount += 1
                cart = cart;
                return;
            }
        }
    }

    $: total = cart.reduce((sum, item) => sum + (item.priceNet * item.tax) * item.amount, 0)

</script>
<strong><h1>Webshop</h1></strong>
<p>There are <b>{cart.length}</b> items in your cart</p>
<div class="product-list">
    {#each products as product}
        <div>
            <div class="image" style="background-image: url({product.image})"></div>
            <h4>{product.productName}</h4>
            <p><b>Nettopreis</b>: ${product.priceNet}</p>
            <button type="button" class="btn btn-primary" on:click={() => addToCart(product)}>Add to cart</button>
        </div>
    {/each}
    <br>
</div>

<div class="cart-list">
    {#each cart as item }
        {#if item.amount > 0}
            <div class="cart-item">
                <img width="50" src={item.image} alt={item.productName}/>
                <div>{item.amount}
                    <button type="button" class="btn btn-primary" on:click={() => plusItem(item)}>+</button>
                    <button type="button" class="btn btn-primary" on:click={() => minusItem(item)}>-</button>
                </div>
                <p>${item.priceNet * item.amount}</p>
            </div>
        {/if}
    {/each}
    <div class="total">
        <b><h4>Total Bruttopreis (inkl. 20% MwSt): $ {total}</h4></b>
        <button class="btn btn-primary mt-2 {cart.length == 0 ? 'disabled' :''}" on:click={buy}>Kaufen</button>
    </div>
</div>

<style>
    .product-list, .cart-item {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
    }

    .image {
        height: 150px;
        width: 150px;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
    }
    .total {
        text-align: right;
    }

    .cart-list {
        border: 2px solid;
        padding: 10px;
    }
</style>
```

## Lager

```js
<svelte:head>
    <title>Lager</title>
</svelte:head>




<script>
    import { onMount } from "svelte";


    let products = [
        {productNumber: "D123RE123D", productName: "HP Pavilion", tax: 1.2, image: "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_88440190/fee_786_587_png", priceNet: 1229, amount: 9},
		{productNumber: "D123RE123A", productName: "MacBook Pro", tax: 1.2, image: "https://assets.mmsrg.com/isr/166325/c1/-/ASSET_MMS_73406796/fee_786_587_png", priceNet: 4454, amount: 5},
		{productNumber: "D123RE123B", productName: "Playstation 5", tax: 1.2, image: "https://www.esdorado.com/3701-large_default/console-sony-playstation-5-ps5.jpg", priceNet: 749.99, amount: 11},
    ]



  let packings = [];
  let endpoint = "http://localhost:8080/stock/packings";

  async function getPackings() {
    const response = await fetch(endpoint);
    packings = await response.json();
  }

  async function packed(packingItemId) {
    const response = await fetch(
      "http://localhost:8080/stock/setPackedForPacking/" + packingItemId,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/hal+json",
        },
      }
    );
    promise = getPackings();
  }

  let promise = getPackings();

  onMount(async function () {
    promise = getPackings();
  });


</script>



<main>

<strong><h1>Lager</h1></strong>
<div class="product-list">
    {#each products as product}
    <div>
            <div class="image" style="background-image: url({product.image})"></div>
            <p>Produktnummer:<b>{product.productNumber}</b></p>
            <p>Produktname:<b>{product.productName}</b></p>
            <p>Lagerbestand:<b>{product.amount}</b></p>
            <p>Nettopreis:<b> ${product.priceNet}</b></p>
    </div>
    {/each}
</div>



<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">PackingList ID</th>
        <th scope="col">Nummer</th>
        <th scope="col">Name</th>
        <th scope="col">Menge</th>
        <th scope="col" />
      </tr>
    </thead>
    <tbody>
      {#await promise}
        <p style="color: blue;>">Warte auf Daten...</p>
      {:then packings}
        {#each packings as packing}
          {#each packing.packingItemList as pack}
            <tr>
              <th scope="row">{packing.orderId}</th>
              <td>{pack.id}</td>
              <td>{pack.productNumber}</td>
              <td>{pack.productName}</td>
              <td>{pack.amount}</td>
              <td>
                <button
                  class="btn btn-sm {pack.packed == true
                    ? 'disabled btn-danger'
                    : 'btn-primary'}"
                  on:click={() => packed(pack.id)}
                  >{pack.packed == true ? "VERPACKT!" : "Verpacken"}</button
                >
              </td>
            </tr>
          {/each}
        {/each}
      {:catch error}
        <p style="color: red;>">{error.message}</p>
      {/await}
    </tbody>
  </table>
</main>
```

## Bestellung

```js
<script>
import { onMount } from "svelte"
  
  let order_id = "O323567890";
  $:endpoint = "http://localhost:8080/api/v1/orders/" + order_id;

  $: if(order_id.length === 10){
    promise = getOrder();
    } 

  async function getOrder(){
    const response = await fetch(endpoint)
    const order = await response.json()

    if (order.errorCode){
      throw new Error(order.errorMessage);
    } else {
    return order;
  }
}

  let promise = getOrder();

	onMount(async function () {
		promise = getOrder();
	})
</script>

<main>
<h1>Bestellung</h1>
Order-Id: <input bind:value={order_id}>
{#if order_id.length !==10}
<p style="color: red">Invalid ID eingegeben!</p>
{/if}

<p>{order_id}</p>

{#await promise}
        <p style="color: blue">Warte auf Daten...</p>
    {:then order}
        <h2>Orders</h2>
        <p>Order-ID: {order.orderID}</p>
        <p>Kunde: {order.customerFirstname}</p>
        <p>Datum: {order.date}</p>
        {#if order.state === "PAYMENT_VERIFIED"}
            <p>Vielden Dank für deine Bestellung!</p>
        {:else}
            <p>Ihre Bestellung wird bearbeitet</p>
        {/if}
    {:catch error}
        <p style="color: red">{error.message}</p>
    {/await}
</main>
```

## Buchhaltung

```js
<script>
    import { onMount } from "svelte";
  
    let orders = [];
    let endpoint = "http://localhost:8080/api/v1/orders/";
  
    async function getOrders() {
      const response = await fetch(endpoint);
      orders = await response.json();
  
      if (orders.errorCode) {
        throw new Error(orders.errorMessage);
      }
      return orders;
    }
  
    async function payed(orderId) {
      const response = await fetch(
        "http://localhost:8080/api/v1/orders/checkpayment/" + orderId,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/hal+json",
          },
        }
      );
      promise = getOrders();
    }
  
    let promise = getOrders();
  
    onMount(async function () {
      promise = getOrders();
    });
  
  </script>
  
  <main>
    <strong><h1>Buchhaltung</h1></strong>
    <div class="Container-fluid">
      <h1 class="text-center">Buchhaltung</h1>
  
      <table class="table table-striped">
        <thead>
          <tr>
            <th scope="col">Order ID</th>
            <th scope="col">Status</th>
            <th scope="col" />
          </tr>
        </thead>
        <tbody>
          {#await promise}
            <p style="color: blue;>">Warte auf Daten...</p>
          {:then orders}
            {#each orders as order}
              <tr>
                <th scope="row">{order.orderID}</th>
                <td>{order.state}</td>
                <td>
                  <button
                    class="btn btn-primary btn-sm {order.state ==
                    'PAYMENT_VERIFIED' || order.state == 'PREPARING_FOR_DELIVERY'
                      ? 'disabled'
                      : ''}"
                    on:click={() => payed(order.orderID)}>Bezahlt</button
                  >
                </td>
              </tr>
            {/each}
          {:catch error}
            <p style="color: red;>">{error.message}</p>
          {/await}
        </tbody>
      </table>
    </div>
  </main>
  
  <style>
  </style>
  ```